module "create_bucket_one" {
  #source       = "../module/"
  source                     = "git::https://gitlab.com/terraform_can/module.git"
  bucket_name                = "conciso-tf-bucket1"
  object_lock_retention_days = 30
  deep_archive_days          = 180
  archive_days               = 90
}
