# tf_branch

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) |  >=1.0.9 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >=4.36.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_create_bucket_one"></a> [create\_bucket\_one](#module\_create\_bucket\_one) | git::https://gitlab.com/terraform_can/module.git | n/a |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
